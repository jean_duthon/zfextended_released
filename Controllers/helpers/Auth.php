<?php
/*
START LICENSE AND COPYRIGHT

 This file is part of ZfExtended library
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**#@+
 * @author Marc Mittag
 * @package ZfExtended
 * @version 2.0
 *
 */
class ZfExtended_Controller_Helper_Auth extends Zend_Controller_Action_Helper_Abstract {
    /**
     * authenticates a user
     *
     * @param  string                   $login 
     * @param  string                   $passwd in clear text
     * @param  string                   $authTableName as taken by Zend_Auth_Adapter_DbTable
     * @param  string                   $identityColumn as taken by Zend_Auth_Adapter_DbTable
     * @param  string                   $credentialColumn as taken by Zend_Auth_Adapter_DbTable
     * @param  string                   $credentialTreatment as taken by Zend_Auth_Adapter_DbTable
     * @return boolean
     */
    public function isValid(string $login, string $passwd, string $authTableName,string $identityColumn,
            string $credentialColumn, string $credentialTreatment) {
        $db = Zend_Registry::get('db');
        $authAdapter = new Zend_Auth_Adapter_DbTable(
            $db,
            $authTableName,
            $identityColumn,
            $credentialColumn,
            $credentialTreatment
        );
        $authAdapter->setIdentity($login)->setCredential($passwd);
        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($authAdapter);
        return $result->isValid()?true:false;
    }
}
