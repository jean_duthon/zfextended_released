<?php
/*
START LICENSE AND COPYRIGHT

 This file is part of ZfExtended library
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**#@+
 * @author Marc Mittag
 * @package ZfExtended
 * @version 2.0
 *
 */
/**
 * Klasse zur Kapselung des Mailversands
 */
class  ZfExtended_Mail {
    const MAIL_TEMPLATE_BASEPATH = '/views/scripts/mail/';
    
    /**
     * @var Zend_View
     */
    protected $view;

    /**
     * @var Zend_Mail
     */
    protected $mail;

    /**
     * setzt ein Flag ob Inhalt schon gesetzt wurde
     * @var boolean
     */
    protected $isContentSet = false;

    /**
     * beinhaltet den template Namen, sofern gesetzt
     * @var string
     */
    protected $template;
    /**
     * @var Zend_Session_Namespace
     */
    protected $_session;
    /**
     * @var integer
     */
    protected $_sendMailLocally = 0;
    /**
     * @var boolean
     */
    protected $_sendBcc = false;
    
    /**
     * disable sending E-Mails completly
     * @var boolean
     */
    protected $sendingDisabled = false;

    /**
     * initiiert das interne Mail und View Object
     *
     * @param boolean initView entscheidet, ob view initialisiert wird
     *      (Achtung: Bei false ist die Verwendung von Mailtemplates mit ZfExtended_Mail nicht möglich)
     *      Default: true
     */
    public function __construct($initView = true) {
        try {
            $this->_session = new Zend_Session_Namespace();
            $this->sendingDisabled = $this->_session->runtimeOptions->sendMailDisabled;
            $this->_sendMailLocally = $this->_session->runtimeOptions->sendMailLocally;
            if(isset($this->_session->runtimeOptions->mail->generalBcc)){
                $this->_sendBcc = true;
            }
        }
        catch (Exception $e) {
        }
        if($initView){
            $this->initView();
        }
        $this->setMail();
    }

    /**
     * setzt das interne Mail Object - z. B. bei verschicken zweier Mails mit der selben Objektinstanz
     * @param Zend_Mail $mail
     */
    public function setMail(){
        $this->mail = new Zend_Mail('utf-8');
    }

    /**
     * initialisiert die View für die Ausgabe des Mail Templates
     */
    protected function initView() {
        $this->view = new Zend_View();
        $this->view->translate = ZfExtended_Zendoverwrites_Translate::getInstance();
        $config = Zend_Registry::get('config');
        $libs = array_reverse($config->runtimeOptions->libraries->order->toArray());
        foreach ($libs as $lib) {
            $this->view->addHelperPath(APPLICATION_PATH.'/../library/'.$lib.
                    '/views/helpers/', $lib.'_View_Helper_');
            $this->view->addScriptPath(APPLICATION_PATH.'/../library/'.$lib.
                    self::MAIL_TEMPLATE_BASEPATH);
        }
        $this->view->addHelperPath(APPLICATION_PATH.'/modules/'.
                Zend_Registry::get('module').'/views/helpers', 'View_Helper_');
        $this->view->addScriptPath(APPLICATION_PATH.'/modules/'.
                Zend_Registry::get('module') . self::MAIL_TEMPLATE_BASEPATH);
    }

    /**
     * Setzt Parameter für das View Object zur Ausgabe des Mail Templates
     * @param array $params
     */
    public function setParameters(array $params) {
        $this->decideIfToThrowInitViewException();
        foreach($params as $key => $val){
            $this->view->assign($key, $val);
        }
    }

    /**
     * Setzt die Mail Anhänge
     * @param array $attachments
     */
    public function setAttachment(array $attachments){
        foreach($attachments as $attachment){
            $at = $this->mail->createAttachment($attachment['body']);
            $at->type        = $attachment['mimeType'];
            $at->disposition = $attachment['disposition'];
            $at->encoding    = $attachment['encoding'];
            $at->filename    = $attachment['filename'];
        }
    }

    /**
     * Setzt den Absender
     * @param string $frommail
     * @param string $fromname
     */
    public function setFrom($frommail, $fromname) {
        $this->mail->setFrom($frommail, $fromname);
    }

    /**
     * Aus den Namen von Controller und Action wird der Templatename generiert.
     * Adaptierung des alten Algorithmus
     */
    protected function detectTemplate() {
        $this->decideIfToThrowInitViewException();
        $trace = debug_backtrace(false);
        $classArr = explode('_', $trace[4]['class']);
        $classArr = array_reverse($classArr);
        return $this->makeTemplateName($classArr[0], $trace[4]['function']);
    }

    /**
     * Generiert aus Klassen- und Funktionsname den Templatenamen
     * @param string $classname
     * @param string $functionname
     * @return string
     */
    protected function makeTemplateName($classname, $functionname) {
        $this->decideIfToThrowInitViewException();
        return strtolower(preg_replace('"Controller$"','',$classname))
                .ucfirst(strtolower(preg_replace('"Action$"','',$functionname)))
                .'.phtml';
    }

    /**
     * Setzt direkt die Mail Content Daten
     * @param string $subject (kann null sein)
     * @param string $textbody
     * @param string $htmlbody
     */
    public function setContent($subject, string $textbody, $htmlbody = '') {
        $this->isContentSet = true;
        $textbody = preg_replace('"\n"', "\r\n", $textbody);
        if(!empty($subject)){
            $this->mail->setSubject($subject);
        }
        $this->mail->setBodyText($textbody);
        if(!empty($htmlbody)){
            $this->addImagesIfExist();
            $this->mail->setBodyHtml($htmlbody, null, Zend_Mime::ENCODING_8BIT);
        }
    }

    /**
     * Sucht nach dem Ordner TemplatenameImages (ohne .phtml) und fügt dort vorhandene Bilder als Attachement hinzu
     * geht alle View Script Pfade durch, und nimmt den ersten in dem ein Images Verzeichnis existiert
     */
    protected function addImagesIfExist(){
        $this->decideIfToThrowInitViewException();
        if(empty($this->template)) {
            return;
        }
        $paths = $this->view->getScriptPaths();
        $imagedir = str_replace('.phtml', 'Images', $this->template);
        $directoryFound = false;
        foreach($paths as $path){
            $directory = $path.'/'.$imagedir;
            $directoryFound = (file_exists($directory) && is_dir($directory));
            if($directoryFound){
                break;
            }
        }
        if(! $directoryFound){
            return; //nichts gefunden.
        }

        $this->mail->setType(Zend_Mime::MULTIPART_RELATED);
        
        foreach (new DirectoryIterator($directory) as $file) {
            if($file->isDot()) continue;
            $filename = $file->getPathname();
            try {
                $imageinfo = getimagesize($filename);
                $imagedata = file_get_contents($filename);
            }
            catch(Exception $e) {
                //Datei kann nicht gelesen werden bzw. ist keien Grafikdatei => ignorieren
                continue;
            }
            $attachment = $this->mail->createAttachment(
                    $imagedata,
                    $imageinfo['mime'],
                    Zend_Mime::DISPOSITION_INLINE,
                    Zend_Mime::ENCODING_BASE64,
                    $file->getFilename());

            $attachment->id = md5($file->getFilename()); //id wird zur cid, welche in html mails für bilder bnötigt wird
        }
    }

    /**
     * Setzt den E-Mail Subject direkt
     * @param string $subject
     */
    public function setSubject(string $subject){
        $this->mail->setSubject($subject);
    }
    /*
     * Entscheidet, ob eine initViewException geworfen werden soll und wirft diese bei Bedarf
     *
     * @throws Zend_Exception
     */
    protected function decideIfToThrowInitViewException() {
        if(!isset($this->view)){
            throw new Zend_Exception('ZfExtended_Mail: Template soll verwendet werden, aber Viewinitialisierung im Konstruktor deaktiviert.');
        }
    }

    /**
     * setzt den Templatenamen der verwendet werden soll
     * weitere Infos siehe setContentByTemplate
     * @see setContentByTemplate
     * @param string $template
     */
    public function setTemplate(string $template){
        $this->decideIfToThrowInitViewException();
        $this->template = $template;
    }

    /**
     * setzt den Templatenamen anhand eines Klassen- und Funktionsnamen
     * @param string $classname
     * @param string $functionname
     */
    public function setTemplateBySignature($classname, $functionname) {
        $this->decideIfToThrowInitViewException();
        $this->template = $this->makeTemplateName($classname, $functionname);
    }

    /**
     * Setzt subject und body anhand des gesetzten Templates, wenn kein Template gesetzt ist, wird es automatisch ermittelt
     * Das Subject wird entweder per setSubject gesetzt (Kompatibilität zur alten mail function)
     * Alternativ kann das Subjet im direkt Template als Variable subject gesetzt werden
     * Wird im Template zusätzlich die Variable textbody gesetzt, wird dieser Wert als Textmail genommen, und die Ausgabe des Templates als HTML Body
     * Ist die Textbody Variable nicht vorhanden, wird die Template Ausgabe als Textbody verwendet
     */
    public function setContentByTemplate() {
        $this->decideIfToThrowInitViewException();
        if(empty($this->template)){
            $templatename = $this->detectTemplate();
        }
        else {
            $templatename = $this->template;
        }
        $body = $this->view->render($templatename);
        $subject = $this->view->subject;
        $textbody = $this->view->textbody;
        if(empty($textbody)){
            //text only Mail
            $this->setContent($subject, $body);
        }
        else {
            //html Mail
            $this->setContent($subject, $textbody, $body);
        }
    }

    /**
     * sendet die E-Mail an den angegebenen Empfänger.
     * Wurde noch kein Content gesetzt, wird das Template aus dem Controller bestimmt
     * - beinhaltet einen Local Mail Hack
     * - versendet an alle per runtimeOptions.mail.generalBcc gelisteten Mailadressen
     *   eine BCC-Mail
     * @param string $toMail
     * @param string $toName
     */
    public function send($toMail, $toName) {
        if(! $this->isContentSet){
            $this->setContentByTemplate();
        }
        
        if($this->sendingDisabled){
            if(ZfExtended_Debug::hasLevel('core', 'mailing')){
                error_log('translate5 disabled mail: '.$this->getSubject().' to '.$toName.' <'.$toMail.'>');
            }
            return;
        }

        //hack, um im lokalen development mails an lokale linux-mailadressen zu senden
        if((int)$this->_sendMailLocally === 1){
            $toMail = preg_replace('"@.+$"', '', $toMail);
        }

        $this->mail->addTo($toMail, $toName);
        if($this->_sendBcc){
            foreach($this->_session->runtimeOptions->mail->generalBcc as $bcc){
                if(preg_match($this->_session->runtimeOptions->defines->EMAIL_REGEX, $bcc)){
                    $this->mail->addBcc($bcc);
                }
            }
        }
        $this->mail->send();
    }

    /**
     * returns the configured subject
     * @return string
     */
    public function getSubject() {
        return $this->mail->getSubject();
    }
    
    /**
     * returns the configured text body
     * @return string
     */
    public function getTextBody() {
        return $this->mail->getBodyText(true);
    }
    
    /**
     * returns the configured html body
     * @return string
     */
    public function getHtmlBody() {
        return $this->mail->getBodyHtml(true);
    }
    
    /**
     * sendet eine Mail an den gegebenen employee, Empfängeradresse und Namen, werden automatisch ausgelesen,
     * ebenso die companyGUID für die Suche nach einem Kundenspezifischen Template
     * @param Zend_Session_Namespace $employee
     */
    public function send2customer(Zend_Session_Namespace $employee){
        $this->decideIfToThrowInitViewException();
        $this->view->addScriptPath(APPLICATION_PATH . '/modules/'.Zend_Registry::get('module') . self::MAIL_TEMPLATE_BASEPATH.$employee->companyGUID.'/');
        $this->send($employee->eMail, $employee->firstname.' '.$employee->surname);
    }
}
